# Artodon

Artodon is a PoC of an art page fully built on top of Mastodon. It's only a
different client for the browser with a gallery look.

The idea of the project is to show the art made by Mastodon users in an
appealing interface like Artstation does. Its main goal is to make a showcase
made by Mastodon community to make people take all the artists at Mastodon
seriously.

It's an art project.

It's something practical.

It looks like this:
![Screenshot](screenshot.png)

## Status

I'm making this as an art project, as a different visualization of the images
and data, so it's made as a read-only webapp.

> It can be developed in the future but it's not the first idea.


## Boring stuff for developers

Artodon is made in ClojureScript because I wanted to learn it. This section
tells you how to develop stuff on it and such. Not very detailed at the moment.

### Development mode

To start the Figwheel compiler, navigate to the project folder and run the
following command in the terminal:

```
lein figwheel
```

Figwheel will automatically push cljs changes to the browser.  Once Figwheel
starts up, you should be able to open the `public/index.html` page in the
browser.

#### Setting up a proxy

This project has no backed at the moment and consumes everything from an API
which doesn't have to have the CORS headers activated. At the same time we want
to run Figwheel to have all the live coding stuff so we need some kind of
magic.

We use Robohydra for that.

Installation:

``` bash
npm install robohydra -g    # may require sudo
```

Robohydra runner (and a plugin!) is included in the project, to run it simply
use:

``` bash
./robohydra_serve
```

The `robohydra_serve` points the mastoart server to
[mastodon.art](https://mastodon.art) by default, you can change the URL in the
file.

> WARNING: We are accessing someone's server. Don't break it. PLEASE.

With that and figwheel running in the background, you'll be able to access
Robohydra server (http://localhost:3000) and make your stuff on it.

### Building for production

```
lein clean
lein package
```

## Interesting stuff

Cool framework
https://github.com/Day8/re-frame

## Thanks and cheers

I have a couple of names on the list but let me finish the project first.
