(ns artodon.core
    (:require-macros [cljs.core.async.macros :refer [go]])
    (:require
      [reagent.core :as r]
      [cljs-http.client :as http]
      [cljs.core.async :refer [<!]]
      ))

(enable-console-print!)

;; -------------------------
;; Data

(def state (r/atom []))
(def querying (atom false))
; TODO Load placeholder images while loading! Make it more fluid


;; -------------------------
;; Download shit
(defn query_list
  [last_id limit]
  (go (let [response (<! (http/get "/api/v1/timelines/tag/mastoart"
                                   {:query-params
                                      {:max_id last_id
                                      :limit limit}}))]
      (reset! state
              (into @state
                    (filter (fn [el] (some #(= (:type %) "image")
                                          (:media_attachments el)))
                            (:body response))))

      (reset! querying false))))

(defn extend_catalog
  []
  (query_list (->> @state
                   (last)
                   (:id))
              (if (first @state)
                  40
                  40)))

(extend_catalog)

;; -------------------------
;; Views

(defn gallery
  [posts]
  [:div.gallery
    (for [p posts]
      (let [account (:account p)
            media   (:media_attachments p)]
      ^{:key (:id p)}
      [:a.gallery-item {:href (:url p) :target "_blank"}
        [:div.account
          [:a {:href (:url account) :target "_blank"}
              [:img.avatar {:src (:avatar_static account)}]]
          [:a {:href (:url account) :target "_blank"}
              [:span.name (or (:display_name account) (:username account))]]]
        [:img.gallery-image
              {:src (:preview_url
                      (first (filter #(= (:type %) "image") media)))}]]))])


(defn create_gallery
  []
  (gallery @state))

;; -------------------------
;; Initialize app

(defn mount-root []
  (r/render [create_gallery] (.getElementById js/document "app")))

(defn init! []
  (mount-root))

;; Test infinite scroll:
(.addEventListener
  js/window
  "scroll"
  (fn
    [e]
   ; (print (.-scrollY js/window))))
    (when (and
            (not @querying)
            (> (.-scrollY js/window) (- (.-scrollMaxY js/window) 2)))
      (reset! querying true)
      (extend_catalog))))
