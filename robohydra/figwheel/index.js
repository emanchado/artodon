var heads                   = require("robohydra").heads,
    RoboHydraHeadProxy      = heads.RoboHydraHeadProxy,
    RoboHydraHeadFilesystem = heads.RoboHydraHeadFilesystem;

// Configuration variables:
// * urlfigwheel: url to proxy to figwheel
// * urlproxy: url to proxy
// * figwheelto: remote URL of the figwheel server. Defaults to 'http:localhost:3449'
// * proxyto: remote URL of the proxy.
// * sethostheader: if the "Host" header should be set to the URL the
//   requests are proxied to. Defaults to yes, set to "no" to avoid it.
exports.getBodyParts = function(conf) {
    "use strict";

    if (conf.urlfigwheel === undefined) {
        console.error("ERROR: Don't have any URL path to serve by figwheel\n" +
                      "(hint: pass the 'urlfigwheel' variable like " +
                      "'robohydra ... urlfigwheel=/')");
        process.exit(1);
    }
    if (conf.figwheelto === undefined) {
      conf.figwheelto = 'http://localhost:3449';
    }
    if (conf.urlproxy === undefined) {
        console.error("ERROR: Don't have any remote URL to serve\n"+
                      "(hint: `urlproxy` variable like " +
                      "'robohydra ... urlproxy=api/')");
        process.exit(1);
    }
    if (conf.proxyto === undefined) {
        console.error("ERROR: I need a URL to proxy to!\n(hint: pass the " +
                      "'proxyto' like 'robohydra ... proxyto=http://...')");
        process.exit(1);
    }

    var heads = [];

    heads.push(new RoboHydraHeadProxy({
        name: 'real-server-proxy',
        mountPath: conf.urlproxy,
        proxyTo: conf.proxyto,
        setHostHeader: (conf.sethostheader !== 'no')
    }));

    heads.push(new RoboHydraHeadProxy({
        name: 'figwheel-proxy',
        mountPath: conf.urlfigwheel,
        proxyTo: conf.figwheelto,
        setHostHeader: (conf.sethostheader !== 'no')
    }));

    return {
        heads: heads
    };
};
